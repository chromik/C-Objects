cmake_minimum_required(VERSION 3.10)
project(CF C)

set(CMAKE_C_STANDARD 90)

add_executable(BINARY
        data_structures/class/binary.c
        data_structures/list/list.c
        data_structures/class/object.c

        globals/global_macros.c
        utils/global_utils.c
        evaluator/var.c
        libraries/mathlib.c
        parser/parse.c
        data_structures/class/name.c
        evaluator/value.c
)