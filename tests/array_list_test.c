//
// Created by chromik on 30.06.2018.
//
#include <stdio.h>
#include <assert.h>
#include "array_list_test.h"
#include "tests.h"
#include "../data_structures/class/object.h"
#include "../data_structures/list/list.h"
#include "../data_structures/class/new.h"
#include "../data_structures/set/set.h"

extern const void *List;
extern const void *Object;

void TEST_ListTest1() {

  void *_list = new(List);
  struct List *list = createListWithSize(_list, 2);
  int i;
  for (i = 0; i < 100; ++i) {
    void *object = new(Object);
    list->addItem(List, object);
  }

  for (i = 0; i < 100; ++i) {
    assert(list->getItem(List, i) == i);
  }

  for (i = 0; i < 100; ++i) {
    void *obj = list->getItem(list, i);

    obj += 1000;
    list->replace(List, i, obj);

    assert(list->getItem(List, i) == i + 1000);
  }

  // Remove 5 items
  list->removeAt(list, 5);
  assert(list->getItem(list, 5) == 1006);
  list->removeAt(list, 23);
  assert(list->getItem(list, 23) == 1025);
  list->removeAt(list, 18);
  assert(list->getItem(list, 18) == 1020);
  list->removeAt(list, 2);
  assert(list->getItem(list, 2) == 1003);
  list->removeAt(list, 47);
  assert(list->getItem(list, 47) == 1052);

  // THIS SHOULD STAY SAME
  assert(list->getItem(list, 1) == 1001);

  // Verify size
  assert(list->getSize(list) == 95);

  // Verify invalid index return NULL
  assert(list->removeAt(list, 95) == NULL);
  assert(list->removeAt(list, 100) == NULL);

  // Verify nothing was really removed
  assert(list->getSize(list) == 95);

  assert(list->removeAt(list, list->getSize(list) - 1) == 1099);
  assert(list->getSize(list) == 94);

  TRACE_INFO("TEST SUCCESSFUL");
}
