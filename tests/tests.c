//
// Created by chromik on 30.06.2018.
//
#include <stdlib.h>
#include <stdio.h>
#include "tests.h"

#include "array_list_test.h"
#include "../globals/global_macros.h"

void __ASSERT_NULL_MACRO(char const *function, char const *file, long line, void *expectedNull) {
  if (expectedNull != NULL) {
    __TRACE_INFO_MACRO(function, file, line, "ASSERT FAILED");
    printf("   value is not NULL.\n");
    exit(1);
  }
#if PRINT_INFO
  else {
    __TRACE_INFO_MACRO(function, file, line, "ASSERT OK");
  }
#endif
}

void __ASSERT_EQUALS_MACRO(char const *function, char const *file, long line, int actualValue, int expectedValue) {
  if (actualValue != expectedValue) {
    __TRACE_INFO_MACRO(function, file, line, "ASSERT FAILED");
    printf("   actual value='%d', expected: '%d'\n", actualValue, expectedValue);
    exit(1);
  }
#if PRINT_INFO
  else {
    __TRACE_INFO_MACRO(function, file, line, "ASSERT OK");
  }
#endif
};

void runAllTests() {
  //TEST_ListTest1();
}
