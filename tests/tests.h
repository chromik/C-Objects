//
// Created by chromik on 30.06.2018.
//
#ifndef CF_TESTS_H
#define CF_TESTS_H

#include "../globals/global_macros.h"
#define ENABLE_TESTS 1
#define PRINT_INFO 1

// MACROS
void __ASSERT_NULL_MACRO(char const *function, char const *file, long line, void *expectedNull);
void __ASSERT_EQUALS_MACRO(char const *function, char const *file, long line, int actualValue, int expectedValue);
#define ASSERT_NULL(X) __ASSERT_NULL_MACRO(__FUNCTION__, __FILE__, __LINE__, X)
#define ASSERT_EQUALS(X, Y) __ASSERT_EQUALS_MACRO(__FUNCTION__, __FILE__, __LINE__, X, Y)

// TEST METHODS
void runAllTests();

#endif //CF_TESTS_H
