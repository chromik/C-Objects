//
// Created by chromik on 12.07.2018.
//

#ifndef CF_MATHLIB_H
#define CF_MATHLIB_H

#include "../globals/global_macros.h"

// Node types
extern const void *Math;

// Library initialization
void initMath(void);

#endif //CF_MATHLIB_H
