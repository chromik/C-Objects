//
// Created by rchromik on 1.7.2018.
//
#ifndef CF_GLOBAL_UTILS_H
#define CF_GLOBAL_UTILS_H

#include <setjmp.h>

static jmp_buf onError;

void error(const char *fmt, ...);

#endif //CF_GLOBAL_UTILS_H
