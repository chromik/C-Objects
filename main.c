#include <stdio.h>
#include <setjmp.h>

#include "data_structures/class/new.h"
#include "data_structures/class/object.h"
#include "data_structures/set/set.h"

jmp_buf onError;

/**
 * THIS IS MAIN FILE BUT NOW IT'S JUST FOR TESTING !!!

 * !!!!!!!!!!! MAIN METHOD FOR INPUT CODE PROCESSING IS PLACED IN "parse.c" main function!!!!!
 *
 */

// TODO list:
/*
 * - in data_structures/circles.c and data_structures/points.c there is abstraction usage with token from argv
 *  - TODO: use this approach but instead of argv use name binary table - IN PROGRESS
 */

// Need to run parsing loop and hadle input source code and process it - DONE

// Update configurations - DONE

// TODO: Implement preprocessor to handle classes without multiple "hacks" that I'am doing now in ANSI C

// TODO: Create tests for what is not covered (almost nothing ;D) for now

// Make consistency beetween object.c, new.c, class.c (need to implement dynamic linkage) - DONE

// Abstraction usage -> DONE
// postfix/infix -> DONE
// TODO: check data structures code, make compilable tests

// TODO: Finish strings
// TODO: finish

// Abstraction test
void abstractionSimpleTest() {
  // Run "CF_DYNAMIC for bag set and "CF_STATIC for static array size set
  void * s = new(Set);
  void * a = add(s, new(Object));
  void * b = add(s, new(Object));
  void * c = new(Object);

  if (contains(s, a) && contains(s, b )) {
    puts("ok");
  }

  if (contains(s, c)) {
    puts("contains?");
  }

  if (differ(a, add(s, a))) {
    puts("differ?");
  }

  if (contains(s, drop(s, a))) {
    puts("drop?");
  }

  delete(drop(s, b));
  delete(drop(s, c));
}

// STRING TEST - NOT WORKING // TODO: FIX (=FINISH) STRINGS
/*void stringSimpleTest() {
  void * a = new(String, "a");
  void * *aa = clone(a);
  void * b = new(String, "b");

  printf("sizeOf(a) == %lu\n", (unsigned long)sizeOf(a));
  if (differ(a, b))
    puts("ok");

  if (differ(a, aa))
    puts("differ?");

  if (a == aa)
    puts("clone?");

  delete(a), delete(aa), delete(b);
}*/

int main() {

  abstractionSimpleTest();

  //stringSimpleTest();

  volatile int errors = 0;

  if (setjmp(onError)) {
    ++errors;
  }

  return errors > 0;
}