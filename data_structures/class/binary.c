//
// Created by chromik on 12.07.2018.
//
#include <string.h>
#include "binary.h"

/**
 * @param key search argument, passed as first argument to cmp if not found, entered as width bytes into table
 * @param _base is the pointer to the beginning of an array.
 * @param nelp is the current number of elements in the array, where each element is width bytes long
 * @param width
 * @param cmp comparision function, must return < == > 0
 * @return returns pointer to table entry containing key, ...., base, base+1...
 */
void *binary(const void *key, void *_base, size_t *nelp, size_t width, int (*cmp)(const void *key, const void *elt)) {
  size_t nel = *nelp;

#define base (*(char**)& _base)

  char *lim = base + nel * width;  // 'key' search argument; 'base' is table ptr (beginning->pointing on first record);
      // 'nel' is (used table records) and 'width' is with of record

  char *high;  // table high limit

  if (nel > 0) {  // at least 1 byte
    for (high = lim - width; base <= high; nel >>= 1) {  // move to the bites to right
      char *mid = base + (nel >> 1) * width;  // get middle
      int c = cmp(key, mid);  // compare with given comparision function

      if (c < 0) {
        high = mid - width;  // set new high limitation lower
      } else if (c > 0) {
        base = mid + width;
        --nel;  // set new low limitation higher
      } else {
        return (void *) mid;  // return pointer to the table record
      }
    }

    // if item not fount
    // move table to be items on the top
    memmove(base + width, base, lim - base);
  }

  ++(*nelp);  // when new item added
  // insert key object as first record and return void *erence to table head (first record)
  return memcpy(base, key, width);

#undef base // MACRO just for this function
}
