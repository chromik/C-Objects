//
// Created by chromik on 11.07.2018.
//
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "binary.h"
#include "name.h"
#include "../../evaluator/value.h"
#include "../../evaluator/var.h" // to create variables
#include "../../parser/parse.h"

/*
 *	Name
 *	maintain a table of Name structures
 */
#ifndef    NAMES
#define    NAMES 16 // initial size of symbol table (can be modified)
#endif

static int cmp(const void *_key, const void *_elt) {
  const char *const *key = _key;
  const struct Name *const *elt = _elt;
  return strcmp(*key, (*elt)->name);
}

static struct Name **search(const char **name) {
  static const struct Name **names;    /* dynamic table */
  static size_t used;
  static size_t max;
  if (used >= max) {  // if there is no space for new one or first initial
    names = (struct Name *) names ? realloc(names, (max *= 2) * sizeof(*names)) :
        malloc((max = NAMES) * sizeof(*names));  // reallocate or allocate new one (=if namespace is empty);
    assert(names);  // verify allocation
  }
  return binary(name, names, &used, sizeof(*names), cmp);  // search is method is used
}

void install(const void *np) {
  const char *name = ((struct Name *) np)->name;
  struct Name **pp = search(&name);
  if (*pp != (void *) name) {
    error("cannot install name twice: %s", name);
  }
  *pp = (struct Name *) np;
}

void *symbol;  // ->last struct Name found by screen() !!!

int screen(const char *name) {
  struct Name **pp = search(&name);
  if (*pp == (void *) name) {    // entered name
    *pp = new(Var, name);
  }
  symbol = *pp;
  return (*pp)->token;
}
