//
// Created by chromik on 16.07.2018.
//
#include <stdio.h>

#include "object.h"
#include "../../evaluator/value.h"

static int Any_differ(const void *_self, const void *b) {
  return 0;  // always return 0, any is equal with every object
}

int main() {
  void *o = new(Object);
  const void *Any = new(Class, "Any", Object, sizeof(o), differ, Any_differ, 0);
  void *a = new(Any);

  puto(Any, stdout);
  puto(o, stdout);
  puto(a, stdout);

  if (differ(o, o) == differ(a, a)) {
    puts("ok");
  }

  if (differ(o, a) != differ(a, o)) {
    puts("not commutative operation");
  }

  delete(o);
  delete(a);
  delete((void *) Any);

  return 0;
}


