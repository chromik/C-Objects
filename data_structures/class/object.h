//
// Created by chromik on 30.06.2018.
//
#ifndef CF_void_H
#define CF_void_H

#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>

#include "object.h"
#include "../../globals/global_macros.h"

struct Object {
  const struct Class *class;  // Objects template (description)
};

struct Class {
  const struct Object _;  // class description
  const char *name;  // class name
  const struct Class *super;  // super class
  size_t size;  // object's size
  void *(*ctor)(void *self, va_list *app);
  void *(*dtor)(void *self);
  int (*differ)(const void *self, const void *b);
  int (*puto)(const void *self, FILE *fp);
};

void *super_ctor(const void *class, void *self, va_list *app);
void *super_dtor(const void *class, void *self);
int super_differ(const void *class, const void *self, const void *b);
int super_puto(const void *class, const void *self, FILE *fp);

extern const void *Object;  // new(Object);

// TODO: SOLVE THIS!!!
//void *new(const void *class, ...);
//void delete(void *self);

const void *classOf(const void *self);
size_t sizeOf(const void *self);

void *ctor(void *self, va_list *app);
void *dtor(void *self);
int differ(const void *self, const void *b);
int puto(const void *self, FILE *fp);

extern const void *Class;  // new(Class, "name", super, size, method, ... 0);

const void *super(const void *self);  // class's superclass

#endif //CF_void_H
