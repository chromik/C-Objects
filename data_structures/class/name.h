//
// Created by chromik on 11.07.2018.
//
#ifndef CF_NAME_H
#define CF_NAME_H

#include "../../globals/global_macros.h"

/**
 * SUPER CLASS IMPLEMENTATION
 * Name
 *	maintain a table of Name structures
 */

struct Name {  // Base structure
  const void *type;    // type for dynamic linkage
  const char *name;    // could be malloc-ed
  int token;  // token when parsing
};

/**
 *	install(& struct Name) - arrange for object to be found later
 *
 *	screen(name)
 *		find a name in a table
 *		if nothing found, enter new(Var, name) into table
 *
 *	@return associated token and pointer to structure
 */
extern void *symbol;    // -> last Name found by screen() !!!

void install(const void *symbol);  // install(& struct Name) - arrange for object to be found later
int screen(const char *name);  // find a name in a table, if nothing found, enter new(Var, name) into table

#endif //CF_NAME_H

