//
// Created by chromik on 12.07.2018.
//
#ifndef CF_BINARY_H
#define CF_BINARY_H

/*
 *	binary()
 *	search and maintain a sorted array
 *
 *	key	search argument, passed as first argument to cmp
 *		if not found, entered as width bytes into table
 *	base	begin of table with *nelp elements of width bytes
 *	cmp()	comparison, must return < == > 0
 *
 *	returns pointer to table entry containing key, e.g., base, base+1...
 */

#include <stddef.h>
#include "../../globals/global_macros.h"

void *binary(const void *key, void *base, size_t *nelp, size_t width, int (*cmp)(const void *key, const void *elt));

#endif //CF_BINARY_H
