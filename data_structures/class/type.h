//
// Created by chromik on 01.07.2018.
//
#ifndef CF_TYPE_H
#define CF_TYPE_H

struct Type {
  size_t size;  // size of object
  void (*dtor)(void *);  // destructor
};

#endif //CF_TYPE_H
