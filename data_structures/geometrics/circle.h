//
// Created by chromik on 10.07.2018.
//
#ifndef CF_CIRCLE_H
#define CF_CIRCLE_H

#include "point.h"

struct Circle {
  const struct Point _;
  int rad;
};

extern const void *Circle;  // new(Circle, x, y, rad)

void initCircle(void);

#endif //CF_CIRCLE_H
