//
// Created by chromik on 10.07.2018.
//
#ifndef CF_POINT_H
#define CF_POINT_H

#include "../class/object.h"
#include "../../globals/global_macros.h"

struct Point {
  const struct Object _;  // Point : Object (class : superclass)
  int x;  // cords x, y
  int y;
};

#define    x(p) (((const struct Point*)(p))->x)
#define    y(p) (((const struct Point*)(p))->y)

void super_draw(const void *class, const void *self);

struct PointClass {
  const struct Class _;  // PointClass : class
  void (*draw)(const void *self);
};

extern const void *Point;  // new(Point, x, y);
extern const void *PointClass;  // this is adding draw method

void initPoint(void);
void draw(const void *self);
void move(void *point, int dx, int dy);


#endif //CF_POINT_H
