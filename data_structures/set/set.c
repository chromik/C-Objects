//
// Created by chromik on 01.07.2018.
//
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "set.h"
#include "../class/object.h"

// TODO: UPDATE TO OBJECT

const void *Set;
const void *Object;

// THIS IS BASIC SET (STATIC SIZE), FOR DYNAMIC SET THERE IS 'bag.c', but they have common interface 'set.h'
#if !defined HEAPSIZE || HEAPSIZE < 1
#define    HEAPSIZE 10
#endif

static int heap[HEAPSIZE];

// TODO: SOLVE THIS!!!
void *new(const void *type, ...) {
  printf("Creating Set with static size (HEAPSIZE=%d)\n", HEAPSIZE);
  int *ptr_heap;  // &heap[1..]
  for (ptr_heap = heap + 1; ptr_heap < heap + HEAPSIZE; ++ptr_heap) {  // verify if there is no pointer to NULL
    if (!*ptr_heap) {
      break;  // there is NULL
    }
  }
  assert(ptr_heap < heap + HEAPSIZE);  // throw exception if heap size is overflowed
  *ptr_heap = HEAPSIZE;  // set heap size
  return ptr_heap;  // return pointer (we called it void *erence, but actually it it's macro and behind void * there is void*
}
// TODO: SOLVE THIS!!!
void delete(void *_item) {
  int *item = _item;
  if (item) {
    assert(item > heap && item < heap + HEAPSIZE);
    *item = 0;
  }
}

void *add(void *_set, const void *_element) {
  int *set = _set;
  const int *element = _element;

  assert(set > heap && set < heap + HEAPSIZE);
  assert(*set == HEAPSIZE);
  assert(element > heap && element < heap + HEAPSIZE);

  if (*element == HEAPSIZE) {
    *((int *) element) = set - heap;
  } else {
    assert(*element == set - heap);
  }

  return (void *) element;
}

void *find(const void *_set, const void *_element) {
  const int *set = _set;
  const int *element = _element;
  assert(set > heap && set < heap + HEAPSIZE);
  assert(*set == HEAPSIZE);
  assert(element > heap && element < heap + HEAPSIZE);
  assert(*element);

  return *element == (set - heap) ? (void *) element : 0;
}

int contains(const void *_set, const void *_element) {
  return find(_set, _element) != 0;
}

void *drop(void *_set, const void *_element) {
  int *element = find(_set, _element);

  if (element) {
    *element = HEAPSIZE;
  }
  return element;
}

int differ(const void *a, const void *b) {
  return a != b;
}
