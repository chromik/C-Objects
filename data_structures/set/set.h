//
// Created by chromik on 01.07.2018.
//
#ifndef CF_SET_H
#define CF_SET_H

#include "../class/object.h"
#include "../../globals/global_macros.h"

// TODO: UPDATE TO OBJECT

extern const void *Set;
/*

struct Set {
  const void * class; */
/* must be first *//*

  unsigned count;
};

struct Object {
  unsigned count; struct Set* in;
};
*/

void *add(void *set, const void *element);
void *find(const void *set, const void *element);
void *drop(void *set, const void *element);
int contains(const void *set, const void *element);

#endif //CF_SET_H

