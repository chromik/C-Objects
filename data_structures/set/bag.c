//
// Created by chromik on 01.07.2018.
//
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "set.h"
#include "../class/new.h"
#include "../class/object.h"

// TODO: UPDATE TO OBJECT

#define this ((struct Set *) _set)

struct Set {
  unsigned count;
};

struct Object {
  unsigned count;
  struct Set *in;
};

// initialize descriptors to can find out how much memory to reserve
static const size_t _Set = sizeof(struct Set);
static const size_t _Object = sizeof(struct Object);
const void *Set = &_Set;  // initialize descriptors to can find out how much memory to reserve
const void *Object = &_Object;  // initialize descriptors to can find out how much memory to reserve

void *add(void *_set, const void *_element) {
  struct Object *element = (void *) _element;
  assert(this);
  assert(element);

  if (!element->in) {
    element->in = set;
  } else {
    assert(element->in == this);
  }

  ++element->count;
  ++set->count;
  return element;
}

void *find(const void *_set, const void *_element) {
  const struct Object *element = _element;
  assert(_set);
  assert(element);
  return element->in == _set ? (void *) element : 0;
}

int contains(const void *_set, const void *_element) {
  return find(_set, _element) != 0;
}

void *drop(void *_set, const void *_element) {
  struct Set *set = _set;
  struct Object *element = find(set, _element);
  if (element) {
    if (--element->count == 0) {
      element->in = 0;
    }
    --set->count;
  }
  return element;
}

unsigned count(const void *_set) {
  const struct Set *set = _set;
  assert(set);
  return set->count;
}

int differ(const void *a, const void *b) {
  return a != b;
}
