//
// Created by chromik on 30.06.2018.
//
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "list.h"
#include "../../globals/global_macros.h"
#include "../../evaluator/value.h"

#define this ((struct List *) _list)

const void *ListClass;
const void *List;

static void *List_createWithSize(void *allocatedMemory, int initialSize) {
  struct List *newList = allocatedMemory;
  if (newList == NULL) {  // allocation of list failed
    return NULL;
  }
  newList->data = (void **) malloc(initialSize * sizeof(void *));
  newList->allocatedSize = initialSize;
  newList->size = 0;
}

static void *List_create(void *allocatedMemory) {
  return List_createWithSize(allocatedMemory, INITIAL_BASE_ARRAY_SIZE);
}

static void *List_ctor(void *_self, va_list *app) {
  struct List *self = _self;
  return List_create(self);
}

unsigned getSize(void *_self) {
  const struct ListClass *class = classOf(_self);
  assert(class->getSize);  // will fail on class = NULL and missing draw also
  class->getSize(_self);
}
static unsigned List_getSize(void *_list) {
  return this->size;
}

int isEmpty(void *_self) {
  const struct ListClass *class = classOf(_self);
  assert(class->isEmpty);  // will fail on class = NULL and missing draw also
  class->isEmpty(_self);
}
static int List_isEmpty(void *_list) {
  return this == NULL || this->data == NULL || this->size == 0;
}

int __isIndexOutOfBounds(int arraySize, int index) {
  if (index < 0 || index >= arraySize) {
    return true;
  } else {
    return false;
  }
}

static void *List_getItem(void *_list, int index) {
  if (!__isIndexOutOfBounds(this->size, index)) {
    return this->data[index];
  } else {
    return NULL;
  }
}
void *getItem(void *_self, int index){
  const struct ListClass *class = classOf(_self);
  assert(class->getItem);  // will fail on class = NULL and missing draw also
  class->getItem(_self, index);
}

void __List_reallocate(void *_list, int newSize) {
  void **newData = (void **) malloc(newSize * sizeof(void *));
  int i;
  for (i = 0; i < this->size; ++i) {
    newData[i] = this->data[i];
  }
  free(this->data);
  this->data = newData;
  this->allocatedSize = newSize;
}

static void List_addLast(void *_list, void *item) {
  if (this->allocatedSize == this->size) {
    int newSize = this->allocatedSize * MULTIPLE_RATIO;
    __List_reallocate(this, newSize);
  }
  this->data[this->size++] = item;
};
void *addLast(void *_self, void *item){
  const struct ListClass *class = classOf(_self);
  assert(class->addLast);  // will fail on class = NULL and missing draw also
  class->addLast(_self, item);
}


static void List_addFirst(void *_list, void *item) {
  if (this->allocatedSize == this->size) {
    int newSize = this->allocatedSize * MULTIPLE_RATIO;
    __List_reallocate(this, newSize);
  }
  int i;
  for (i = 0; i < this->size - 1; ++i) {
    this->data[i + 1] = this->data[i];
  }
  this->data[0] = item;
}
void *addFirst(void *_self, void *item){
  const struct ListClass *class = classOf(_self);
  assert(class->addFirst);  // will fail on class = NULL and missing draw also
  class->addFirst(_self, item);
}

static void List_addItem(void *_list, void *item) {
  addLast(_list, item);
}
void *addItem(void *_self, void *item){
  const struct ListClass *class = classOf(_self);
  assert(class->addItem);  // will fail on class = NULL and missing draw also
  class->addItem(_self, item);
}

static void List_addAll(void *_list, struct List *addList) {
  int i;
  for (i = 0; i < addList->size; ++i) {
    addLast(this, addList->data[i]);
  }
}
void *addAll(void *_self, struct List *addList){
  const struct ListClass *class = classOf(_self);
  assert(class->addAll);  // will fail on class = NULL and missing draw also
  class->addAll(_self, addList);
}

static void *List_removeAt(void *_list, int index) {
  if (!__isIndexOutOfBounds(this->size, index)) {
    void *toReturn = this->data[index];
    int i;
    for (i = index; i < this->size - 1; ++i) {
      this->data[i] = this->data[i + 1];
    }
    this->data[--this->size] = NULL;
    return toReturn;
  } else {
    return NULL;
  }
}
void *removeAt(void *_self, int index){
  const struct ListClass *class = classOf(_self);
  assert(class->removeAt);  // will fail on class = NULL and missing draw also
  class->removeAt(_self, index);
}

static void *List_removeItem(void *_list, void *item) {
  int indexToRemove = -1;
  if (this != NULL && this->data != NULL) {
    int i;
    for (i = 0; i < this->size; ++i) {
      if (this->data[i] == item) {
        indexToRemove = i;
      }
    }
  }
  return removeAt(this, indexToRemove);
}
void *removeItem(void *_self, void *item){
  const struct ListClass *class = classOf(_self);
  assert(class->removeItem);  // will fail on class = NULL and missing draw also
  class->removeItem(_self, item);
}

static void *List_removeFirst(void *_list) {
  return removeAt(_list, 0);
}
void *removeFirst(void *_self){
  const struct ListClass *class = classOf(_self);
  assert(class->removeFirst);  // will fail on class = NULL and missing draw also
  class->removeFirst(_self);
}

static void *List_removeLast(void *_list) {
  return removeAt(_list, getSize(this) - 1);
}
void *removeLast(void *_self){
  const struct ListClass *class = classOf(_self);
  assert(class->removeLast);  // will fail on class = NULL and missing draw also
  class->removeLast(_self);
}

static void *List_getFirst(void *_list) {
  return getItem(_list, 0);
}
void *getFirst(void *_self){
  const struct ListClass *class = classOf(_self);
  assert(class->getFirst);  // will fail on class = NULL and missing draw also
  class->getFirst(_self);
}

static void *List_getLast(void *_list) {
  return getItem(_list, getSize(this) - 1);
}
void *getLast(void *_self){
  const struct ListClass *class = classOf(_self);
  assert(class->getLast);  // will fail on class = NULL and missing draw also
  class->getLast(_self);
}

static void List_replace(void *_list, int index, void *item) {

  if (!__isIndexOutOfBounds(this->size, index)) {
    this->data[index] = item;
  } else {
    printf("INDEX OUT OF BOUNDS EXCEPTION");
    exit(1);
  }
}
void *replace(void *_self, int index, void *item){
  const struct ListClass *class = classOf(_self);
  assert(class->replace);  // will fail on class = NULL and missing draw also
  class->replace(_self, index, item);
}

static void List_toString(void *_list) {
  printf("[");
  size_t i;
  for (i = 0; i < this->size; ++i) {
    if (i) {
      printf(",");
    }
    printf("%g", doVal(this->data[i]));
  }
  printf("]\n");
}
void toString(void *_self){
  const struct ListClass *class = classOf(_self);
  assert(class->toString);  // will fail on class = NULL and missing draw also
  class->toString(_self);
}

// List Class
static void *ListClass_ctor(void *_self, va_list *app) {
  struct ListClass *self = super_ctor(ListClass, _self, app);
  typedef void (*voidf)();
  voidf selector;

#ifdef va_copy
  va_list ap;
  va_copy(ap, *app);
#else
  va_list ap = * app;
#endif

  while ((selector = va_arg(ap, voidf))) {

    voidf method = va_arg(ap, voidf);

    if (selector == (voidf) getItem) {
      *(voidf *) &self->getItem = method;
    } else if (selector == (voidf) addItem) {
      *(voidf *) &self->addItem = method;
    } else if (selector == (voidf) addFirst) {
      *(voidf *) &self->addFirst = method;
    } else if (selector == (voidf) addLast) {
      *(voidf *) &self->addLast = method;
    } else if (selector == (voidf) addAll) {
      *(voidf *) &self->addAll = method;
    } else if (selector == (voidf) getSize) {
      *(voidf *) &self->getSize = method;
    } else if (selector == (voidf) isEmpty) {
      *(voidf *) &self->isEmpty = method;
    } else if (selector == (voidf) replace) {
      *(voidf *) &self->replace = method;
    } else if (selector == (voidf) removeFirst) {
      *(voidf *) &self->removeFirst = method;
    } else if (selector == (voidf) removeLast) {
      *(voidf *) &self->removeLast = method;
    } else if (selector == (voidf) getFirst) {
      *(voidf *) &self->getFirst = method;
    } else if (selector == (voidf) getLast) {
      *(voidf *) &self->getLast = method;
    } else if (selector == (voidf) toString) {
      *(voidf *) &self->toString = method;
    }
  }

#ifdef va_copy
  va_end(ap);
#endif

  return self;
}

void initList(void) {
  if (!ListClass) {
    ListClass = new(Class, "ListClass", Class, sizeof(struct ListClass), ctor, ListClass_ctor, 0);
  }
  if (!List) {
    List = new(ListClass, "List", Object, sizeof(struct List), ctor, List_ctor, List_getItem, List_addItem,
               List_addFirst, List_addLast, List_removeItem, List_removeAt, List_addAll, List_getSize, List_isEmpty,
               List_replace, List_removeFirst, List_removeLast, List_getFirst, List_getLast, List_toString, 0);
  }
}

void _destroy(void *_list) {
  while (!isEmpty(this)) {
    free(List_removeLast(this));
  }
  free(this);
}

#undef this