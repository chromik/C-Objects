//
// Created by chromik on 30.06.2018.
//
#ifndef CF_LIST_H
#define CF_LIST_H

// TODO: UPDATE TO OBJECT

#include <stdlib.h>
#include "../class/object.h"
#include "../../globals/global_macros.h"

struct List {
  const struct Object _;  // Point : Object (class : superclass)

  const void *class;  // Class must be first!
  void **data;
  unsigned size;
  int allocatedSize;
};

struct ListClass {
  const struct Class _;  // ListClass : class
  void *(*getItem)(void *this, int index);
  void *(*addItem)(void *this, void * item);
  void (*addFirst)(void *this, void *item);
  void (*addLast)(void *this, void *item);
  void *(*removeItem)(void *this, void *item);
  void *(*removeAt)(void *this, int index);
  void (*addAll)(void *this, struct List *list);
  unsigned (*getSize)(void *this);
  int (*isEmpty)(void *this);
  void *(*replace)(void *this, int index, void *item);
  void *(*removeFirst)(void *this);
  void *(*removeLast)(void *this);
  void *(*getFirst)(void *this);
  void *(*getLast)(void *this);
  void (*toString) (void *this);
};


extern const void *List;
extern const void *ListClass;

#define INITIAL_BASE_ARRAY_SIZE 620
#define MULTIPLE_RATIO 2

#endif //CF_LIST_H
