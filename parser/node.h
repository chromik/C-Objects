//
// Created by chromik on 09.07.2018.
//
#ifndef CF_NODE_H
#define CF_NODE_H

#include "parse.h"

static enum tokens {
  A, B, C
} token;

struct Node {
  enum tokens token;
  struct Node *left;  // left child node
  struct Node *right;  // right child node
};

#endif //CF_NODE_H
