//
// Created by rchromik on 1.7.2018.
//
#include <ctype.h>
#include <errno.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parse.h" /* defines NUMBER */
#include "../evaluator/value.h"
#include "../evaluator/var.h"
#include "../data_structures/class/name.h"
#include "../data_structures/list/list.h"
#include "../libraries/mathlib.h"
#include "../utils/global_utils.h"

// all possible alphanumeric symbols
#define ALNUM "ABCDEFGHIJKLMNOPQRSTUVWXYZ" \
  "abcdefghijklmnopqrstuvwxyz" \
  "_" \
  "0123456789"

static void initNames(void) {

  static const struct Name names[] = {
      {0, "var", ASSIGN},
      {0}
  };

  const struct Name *np;

  for (np = names; np->name; ++np) {
    install(np);
  }
}

static enum tokens token;  // current input symbol
static double number;  // if NUMBER: numerical value

static enum tokens scan(const char *buf) {  // return token = next input symbol

  static const char *bp;

  if (buf) {
    bp = buf;  // new input line
  }

  while (isspace(*bp & 0xff)) {
    ++bp;
  }

  if (isdigit(*bp & 0xff) || *bp == '.') {
    errno = 0;
    token = NUMBER;
    number = strtod(bp, (char **) &bp);
    if (errno == ERANGE) {
      error("bad value: %s", strerror(errno));
    }

  } else if (isalpha(*bp & 0xff) || *bp == '_') {
    char buf[BUFSIZ];
    int len = strspn(bp, ALNUM);

    if (len >= BUFSIZ) {
      error("name too long: %-.10s...", bp);
    }
    strncpy(buf, bp, len), buf[len] = '\0', bp += len;
    token = screen(buf);
  } else {
    token = *bp ? *bp++ : 0;
  }
  return token;
}

/**
 * factor :  +
 *			 -
 *			 NUMBER
 * @return sum result
 */
static void *sum(void);

/**
 *
 * @return result
 */
static void *factor(void) {
  void *result;
  void **vectorResult;

  switch ((int) token) {
    case '+':scan(0);
      return factor();

    case '-':scan(0);
      return new(Minus, factor());

    default:error("bad factor: '%c' 0x%x", token, token);

    case NUMBER:result = new(Value, number);
      break;

    case CONST:
    case VAR:result = symbol;
      break;

    case MATH: {
      const struct Name *fp = symbol;

      if (scan(0) != '(') {
        error("expecting (");
      }

      scan(0);
      result = new(Math, fp, sum());
      if (token != ')') {
        error("expecting )");
      }
      break;
    }

    case '(':
      scan(0);
      result = sum();
      if (token != ')') {
        error("expecting )");
      }
      break;


    case VECTOR_START:
      vectorResult = malloc(BUFSIZ * sizeof(void));
      int i;
      for (i = 0;; ++i) {
        scan(0);
        vectorResult[i] = sum();
        if (token == VECTOR_END) {
          break;
        } else if (token != ',') {
          error("expecting ,");
        }
      }
      scan(0);
      return vectorResult[i]; // TODO: return vector instead of last item of it
  }
  scan(0);
  return result;
}

/**
 * product : factor { *|/ factor }...
 * @return result
 */
static void *product(void) {
  void *result = factor();
  const void *type;
  for (;;) {
    switch ((int) token) {
      case '*':
        type = Mult;
        break;

      case '/':
        type = Div;
        break;

      default:
        return result;
    }
    scan(0);
    result = new(type, result, factor());
  }
}

/**
 *	sum : product { +|- product }...
 *
 * @return result
 */
static void *sum(void) {
  void *result = product();
  const void *type;

  for (;;) {
    switch ((int) token) {
      case '+': // adding positive number
        type = Add;
        break;
      case '-': // adding negative number,
        type = Sub;
        break;
      default: // return value number
        return result;
    }
    scan(0);
    result = new(type, result, product());
  }
}

/**
 * statement : let VAR = sum
 * @return sum
 */
static void *statement(void) {
  void *result;
  switch (token) {
    case ASSIGN:
      if (scan(0) != VAR) {
        error("bad assignment");
      }
      result = symbol;
      if (scan(0) != '=') {
        error("expecting =");
      }
      scan(0);
      return new(Assign, result, sum());
    default:return sum();
  }
}

/*
 *	sum \n ...
 */
static jmp_buf onError;

int main(void) {

  volatile int errors = 0;
  char buffer[BUFSIZ];  /// allocate buffer with static size

  initNames();
  initConst();
  initMath();

  // --- THIS IS ERROR HANDLING BLOCK ---
  if (setjmp(onError)) {  // if there is some error in program than it will jump here
    ++errors;  // increase number of errors
  }
  // and continue with parsing...
  // --- END OF ERROR HANDLING BLOCK ---

  while (fgets(buffer, sizeof(buffer), stdin)) {
    if (scan(buffer)) {  // while there is ('\n', ' ', '.'. [digit], [alpha], '_')

      void *_statement = statement();
      /****************************************************
       * NUMBER = 'n', // literal constant
       * CONST = 'c',				constant name
       * MATH = 'm',				library function name
       * VAR = 'v',				variable name
       * ASSIGN = 'l'				var VAR = sum
       ****************************************************/
      if (token) {  // if there is token ('n' 'c' 'm' 'v' 'l', sum, new object) it's error because this is not processing them
        error("trash after sum");
      }

      process(_statement);  // otherwise process (postfix, infix, assignation ...)
      delete(_statement);  // and delete void *erence fore processed element
    }
  }
  return errors > 0;  // return it there was some error;
}

void error(const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap), putc('\n', stderr);  // print error message with arguments for formatting
  va_end(ap);
  longjmp(onError, 1);  // jump to the start parsing
}
