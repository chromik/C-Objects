//
// Created by rchromik on 1.7.2018.
//
#ifndef CF_PARSE_H
#define CF_PARSE_H

#include "../globals/global_macros.h"

// SYMBOLS
enum tokens {  // cannot clash with operators
  NUMBER = 'n', // literal constant
  CONST = 'c', // constant name
  MATH = 'm', // library function name
  VAR = 'v', // variable name
  VECTOR_START = '[',
  VECTOR_END = ']',

  ASSIGN = 'l'   // var VAR = sum
};

// ERROR RECOVERY
void error(const char *fmt, ...);

#endif //CF_PARSE_H
