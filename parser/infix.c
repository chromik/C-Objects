//
// Created by chromik on 10.07.2018.
//
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "parse.h"
#include "../evaluator/value.h"

/*
 *	infix driver
 */
struct Type {
  const char *name;  // node's name
  char rank;
  char rpar;
  void *(*new)(va_list ap);
  void (*exec)(const void *tree, int rank, int par);
  void (*delete)(void *tree);
};

void *new(const void *type, ...) {
  va_list ap;
  void *result;
  assert(type && ((struct Type *) type)->new);
  va_start(ap, type);
  result = ((struct Type *) type)->new(ap);
  *(const struct Type **) result = type;
  va_end(ap);
  return result;
}

void delete(void *tree) {
  assert(tree && *(struct Type **) tree && (*(struct Type **) tree)->delete);
  (*(struct Type **) tree)->delete(tree);
}

void process(const void *tree) {
  putchar('\t');
  exec(tree, (*(struct Type **) tree)->rank, 0);
  putchar('\n');
}

// NUMBER
struct Val {
  const void *type;
  double value;
};

static void *mkVal(va_list ap) {
  struct Val *node = (struct Val *) malloc(sizeof(struct Val));
  assert(node);
  node->value = va_arg(ap, double);
  return node;
}

static void doVal(const void *tree, int rank, int par) {
  printf("%g", ((struct Val *) tree)->value);
}

// Unary operators
struct Un {
  const void *type;
  void *arg;
};

static void *mkUn(va_list ap) {
  struct Un *node = (struct Un *) malloc(sizeof(struct Un));
  assert(node);
  node->arg = va_arg(ap, void *);
  return node;
}

static void doUn(const void *tree, int rank, int par) {
  const struct Type *type = *(struct Type **) tree;
  printf("%s ", type->name);
  exec(((struct Un *) tree)->arg, type->rank, 0);
}

static void freeUn(void *tree) {
  delete(((struct Un *) tree)->arg);
  free(tree);
}

// Binary operatos
struct Bin {
  const void *type;
  void *left;
  void *right;
};

static void *mkBin(va_list ap) {
  struct Bin *node = (struct Bin *) malloc(sizeof(struct Bin));
  assert(node);
  node->left = va_arg(ap, void *);
  node->right = va_arg(ap, void *);
  return node;
}

static void doBin(const void *tree, int rank, int par) {
  const struct Type *type = *(struct Type **) tree;
  par = type->rank < rank || (par && type->rank == rank);
  if (par) {
    putchar('(');
  }
  exec(((struct Bin *) tree)->left, type->rank, 0);
  printf(" %s ", type->name);
  exec(((struct Bin *) tree)->right, type->rank, type->rpar);
  if (par) {
    putchar(')');
  }
}

static void freeBin(void *tree) {
  delete(((struct Bin *) tree)->left);
  delete(((struct Bin *) tree)->right);
  free(tree);
}

// Types
static struct Type _Add = {"+", 1, 0, mkBin, doBin, freeBin};
static struct Type _Sub = {"-", 1, 1, mkBin, doBin, freeBin};
static struct Type _Mult = {"*", 2, 0, mkBin, doBin, freeBin};
static struct Type _Div = {"/", 2, 1, mkBin, doBin, freeBin};
static struct Type _Minus = {"-", 3, 0, mkUn, doUn, freeUn};
static struct Type _Value = {"", 4, 0, mkVal, doVal, free};

// Initialize descriptors to can find out how much memory to reserve
const void *Add = &_Add;
const void *Sub = &_Sub;
const void *Mult = &_Mult;
const void *Div = &_Div;
const void *Minus = &_Minus;
const void *Value = &_Value;
