//
// Created by chromik on 09.07.2018.
//
#ifndef CF_VALUE_H
#define CF_VALUE_H

#include <stdarg.h>

#include "../globals/global_macros.h"

struct Type {
  void *(*new)(va_list ap);
  double (*exec)(const void *tree);
  void (*delete)(void *tree);
};

double exec(const void *tree);

struct Bin {
  const void *type;
  void *left;
  void *right;
};

#define    left(tree)    (((struct Bin*)tree)->left)
#define    right(tree)    (((struct Bin*)tree)->right)

void *mkBin(va_list ap);
void freeBin(void *tree);

/* Node types */
const void *Minus;
const void *Value;
const void *Mult;
const void *Div;
const void *Add;
const void *Sub;
const void **Vector;

/* Tree management */
/// TODO: SOLVE THIS!!!
void *new(const void *type, ...);
// TODO: SOLVE THIS!!!
void delete(void *tree);

void process(const void *tree);

double doVal(const void *tree);

#endif //CF_VALUE_H
