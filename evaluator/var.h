//
// Created by chromik on 12.07.2018.
//

#ifndef CF_VAR_H
#define CF_VAR_H

#include "../globals/global_macros.h"

// Node types
const void *Var;
const void *Assign;

// Initialize
void initConst(void);

#endif //CF_VAR_H
