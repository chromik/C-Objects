//
// Created by chromik on 30.06.2018.
//

#ifndef CF_GLOBAL_MACROS_H
#define CF_GLOBAL_MACROS_H

// PRIMITIVE DATA TYPES
// for (non-objective) int
#define boolean int
#define true 1
#define false 0

// MACROS
// Useful for error printing or debugging - prints function name, file and line with message
void __TRACE_INFO_MACRO(char const *function, char const *file, long line, char const *message);
#define TRACE_INFO(ARG) __TRACE_INFO_MACRO(__FUNCTION__, __FILE__, __LINE__, ARG)

#endif //CF_GLOBAL_MACROS_H
