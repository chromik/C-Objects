//
// Created by chromik on 30.06.2018.
//

#include <stdio.h>
#include "global_macros.h"

void __TRACE_INFO_MACRO(char const *function, char const *file, long line, char const *message) {
  printf("[%s]\t %s:%ld [%s]\n", message, file, line, function);
}
